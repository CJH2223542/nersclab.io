# Managing Spin Apps with Helm

One challenge associated with running complex workloads in Kubernetes is
managing and tracking the complete state of the workload, with all of its
associated containers, services, and orchestration. [Helm](https://helm.sh) is
a tool designed to simplify the process of managing such complex workflows,
specifically using a feature called
[charts](https://helm.sh/docs/topics/charts/).

[This slide deck (PDF)](https://drive.google.com/file/d/1iy9bqS_GTtb2s5OzjnBRmpp0YnzbtJyZ/view)
describes NERSC user
[Valerie Hendrix](https://crd.lbl.gov/departments/data-science-and-technology/uss/staff/valerie-hendrix/)'s
process for implementing and using Helm charts to manage workflows in Spin.

[This video (MP4)](https://drive.google.com/file/d/10myW2FIwRkkD8bvwUwdfDgmefBjLsFVb/view)
shows Val demonstrating the approach for NERSC staff.
