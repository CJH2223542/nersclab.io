#!/bin/bash
#SBATCH -J myjob
#SBATCH -A <your account name>  # e.g., m1111
#SBATCH -q regular
#SBATCH -t 6:00:00        
#SBATCH -N 2           
#SBATCH -C gpu
#SBATCH -G 8 
#SBATCH --exclusive
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err

module load vasp/6.2.1-gpu

export OMP_NUM_THREADS=1
export OMP_PLACES=threads
export OMP_PROC_BIND=spread

srun -n 8 -c 32 --cpu-bind=cores --gpu-bind=none -G 8 vasp_std

