## Issue
(This issue type is used for problems related to NERSC documentation including errors in examples, spelling errors, broken links, in-accurate documentation)

## Link
(Link to user documentation: https://docs.nersc.gov/)

## Describe the Issue 

(Please provide a detailed summary of the issue)

## Screenshots

(Paste a screenshot if applicable)


## I confirm there is no existing [issue](https://gitlab.com/NERSC/nersc.gitlab.io/-/issues)?

- [ ] I confirm 

## Would you be willing to address this issue by [contributing](https://gitlab.com/NERSC/nersc.gitlab.io/-/blob/main/CONTRIBUTING.md) to NERSC documentation?

- [ ] Yes
- [ ] No

/label ~issue ~triage
