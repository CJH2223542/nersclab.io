#!/bin/bash
#SBATCH -J test
#SBATCH -q flex
#SBATCH -N 2 
#SBATCH -C knl
#SBATCH -t 48:00:00
#SBATCH -o %x-%j.out
#SBATCH -e %x-%j.err
#SBATCH --time-min=2:00:00

#user settings
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
export OMP_NUM_THREADS=8

#for c/r with mana
module load mana

#checkpointing once every hour
mana_coordinator -i 3600

#restart job from checkpoint files
srun -n 16 -c 32 --cpu-bind=cores mana_restart


