# Gitlab

[Gitlab](https://about.gitlab.com/) is a DevOps platform to allow software development teams to collaborate together by
hosting code in a source repository and providing infrastructure for automated builds, integration and verification of code using
Continuous Integration/Continuous Delivery (CI/CD). The [Gitlab Project](https://gitlab.com/gitlab-org/gitlab)
is open source and actively maintained by Gitlab Inc.

## Access

NERSC provides a GitLab service for users available at https://software.nersc.gov/. You can access this service
with your NERSC credentials.  
